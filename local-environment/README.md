# README #

this README will instruct you to start a local accezz environment. 

## prerequisites 

 
### serup dns resolving
for local environemnt we are using `local.accezz.io` domain, this domain should resolved from your browser. 

you can do it by
option 1. setup your /etc/hosts ( c:\Windows\System32\drivers\etc\hosts ) file to resolve local.accezz.io & accezz-mgmt.local.accezz.io to localhost. * not recomanded *

```
##
# Host Database
#
# localhost is used to configure the loopback interface
# when the system is booting.  Do not change this entry.
##
127.0.0.1	localhost
255.255.255.255	broadcasthost
::1             localhost

127.0.0.1       local.accezz.io
127.0.0.1       accezz-mgmt.local.accezz.io
```

option 2. install a local dns server and config it
osx users can check this : https://gist.github.com/jed/6147872

### update your browser with the self-signed ssl certificate

on Mac : 
```
open /Applications/Utilities/Keychain\ Access.app ssl.crt

```

// TODO on Windows : 


### setup db, redis, consul services

from `local-environemnt` directory, run the following: 

```
docker-compose -f docker-compose.env.yml up

```

This will create the db, and migrate it to the latest. 


## Accezz environment.

from `local-environemnt` directory, run the following: 

```
docker-compose -f docker-compose.accezz.yml up

```


## connnector

to start a connector, get the OTP from the UI and update the docker-compose.connector.yml file. 

```
docker-compose -f docker-compose.connector.yml up

```

## applications : 

you can use the docker-compose.applications.yml file to start 2 application. 

1. gotour - listening on 3999
2. files. - listening on 8080


```
docker-compose -f docker-compose.applications.yml up

```


### generating self-signed ssl 
```bash
openssl req -new -newkey rsa:2048 -sha1 -days 3650 -nodes -x509 -keyout ssl.key -out ssl.crt -config openssl.cnf
```