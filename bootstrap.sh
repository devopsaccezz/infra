#!/usr/bin/env bash

# This is a temporary provisioning file for the Accezz.io development environment VMs/Instances. It uses non-declarative Bash shell script to prepare a
# Ubuntu machine running Docker Engine and Docker Compose. 

echo ****************** Provisioning of Accezz.io PoD Image Start **************************
#sleep 15

sudo apt-get update

# Install curl
sudo apt-get install -y curl

# Install Docker Engine
echo Installing Docker Engine
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'
sudo apt-get update
sudo apt-cache policy docker-engine
sudo apt-get install -y docker-engine
sudo systemctl status docker

# Install Docker Compose
echo Installing Docker Compose
sudo curl -L https://github.com/docker/compose/releases/download/1.9.0/docker-compose-`uname -s`-`uname -m` > /tmp/docker-compose
sudo chmod +x /tmp/docker-compose
sudo cp /tmp/docker-compose /usr/local/bin/docker-compose

# Pull Accezz.io Containers from the Docker Hub
sudo docker login -e dev@accezz.io -u accezziohub -p *Accezz*
sudo docker pull accezziohub/accezzio:core-server-latest
sudo docker logout

# Prepare the directory structure for running the environment
if [ -f /tmp/accezz-pod.yml ]
then
    echo "Moving the Accezz environment files to /usr/local/accezz"
    sudo mkdir /usr/local/accezz
    sudo cp /tmp/accezz-pod.yml /usr/local/accezz/accezz-pod.yml
fi

echo ****************** Provisioning of Accezz.io PoD Image End **************************
