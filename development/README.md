<<<<<<< HEAD
How to set up a development environment
====

# Install postgresql server on your local machine. 
  # Create the user `accpostgres` with the password `accpostgres123`. Give it full permissions including create database permission by running the 
  following query `ALTER USER accpostgres CREATEDB;`
  # Run `../install-db.sh localhost` 
# Edit the `.env` file if needed
# `haproxy` container is used in case you would like to stubbify one of the containers and point it to a service running on
   your developer machine instead. In order to do so, modify the `accezz-dev-compose.yml` file and replace the image for that container to
   `haproxy` and add a volume for it with the following mapping: `./haproxy.conf:/usr/local/etc/haproxy/haproxy.cfg`. Make sure that `haproxy.conf` 
   is suitable for your needs.
# Run `docker-compose -f accezz-dev-compose.yml up` to start the environment, watch the output for errors. Your local environment will now be avaliable
  through `https://local.accezz.io`.
=======
How to set up a development environment
====

Install postgresql server on your local machine.
Create the user `accpostgres` with the password `accpostgres123`.
 `sudo usermod -a -G postgres accpostgres`

use `sudo -u postgres createuser -P -s -e accpostgres` and enter the password `accpostgres123`

Run `environment/idaz/remote_provisioners/initialize-db.sh localhost` 
  
Edit the `.env` file if needed

`haproxy` container is used in case you would like to stubbify one of the containers and point it to a service running on
   your developer machine instead. In order to do so, modify the `accezz-dev-compose.yml` file and replace the image for that container to
   `haproxy` and add a volume for it with the following mapping: `./haproxy.conf:/usr/local/etc/haproxy/haproxy.cfg`. Make sure that `haproxy.conf` 
   is suitable for your needs.
   
Run `docker-compose -f accezz-dev-compose.yml up` to start the environment, watch the output for errors. 

Your local environment will now be avaliable
  through `https://local.accezz.io`.
>>>>>>> 8ab3f7f51afd3f8a8a559a18060fe89047aec1a5
