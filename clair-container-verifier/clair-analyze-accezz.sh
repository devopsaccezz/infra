#!/usr/bin/env bash
#/bin/bash

# Login to Docker Hub
./hyperclair-login.exp

# Pull the image from Docker Hub
hyperclair pull accezziohub/accezzio:core-server-latest

# Push the image to Clair
hyperclair push accezziohub/accezzio:core-server-latest

# Perform analysis of the image (trigger Clair)
hyperclair analyse accezziohub/accezzio:core-server-latest

# Generate an HTML report
hyperclair report accezziohub/accezzio:core-server-latest

hyperclair logout
