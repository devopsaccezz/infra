# Define an EBS volume for this specific pod to hold the logs
resource "aws_ebs_volume" "logstore" {
    #region = "${var.pod_region}"
    availability_zone = "${var.pod_region}a"
    size = 50
    tags {
        Name = "${var.pod_name}-logstore"
    }
    lifecycle {
         #prevent_destroy = true
         create_before_destroy = true
    }
}

# Define an ES3 bucket for this specific pod to hold the dynamically-generated SSL certificates
resource "aws_s3_bucket" "certstore" {
    bucket = "certstore.${var.pod_name}.accezz.io.2"
    acl = "private"
    provider = "aws"
    tags {
        Name = "${var.pod_name}-certstore.1"
    }
    lifecycle {
         #prevent_destroy = true
         create_before_destroy = true
    }    
}

