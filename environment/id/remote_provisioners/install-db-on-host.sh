#!/bin/bash

# Installing Postgresql 9.6
echo "Installing Postgresql 9.6"
sudo add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main"
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install -y postgresql-9.6

# Create our user account
echo "Creating superuser"
sudo -u postgres bash -c "psql -c \"CREATE USER accpostgres WITH PASSWORD 'accpostgres123';\""
sudo -u postgres bash -c "psql -c \"ALTER USER accpostgres WITH SUPERUSER;\""

# Allow listening to all interfaces. TODO: This needs to be hardened in the future
# listen_addresses = '*'
echo "Listening to all machine interfaces"
sudo sed -i -e"s/^#listen_addresses =.*$/listen_addresses = '*'/" /etc/postgresql/9.6/main/postgresql.conf

# Allow connecting from all IPs. TODO: This needs to be hardened in the future
# host  all  all 0.0.0.0/0 md5
echo "Allowing login from all hosts"
sudo sh -c 'echo "host    all    all    0.0.0.0/0    md5" >> /etc/postgresql/9.6/main/pg_hba.conf'

echo "Restarting service"
sudo /etc/init.d/postgresql restart
