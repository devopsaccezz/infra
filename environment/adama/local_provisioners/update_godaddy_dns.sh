#!/bin/bash


# ------------------------------------------------------------------
# update_godaddy_dns.sh
#          Dynamically update GoDaddy DNS Records for a specified domain
#          record with GoDaddy API
# ------------------------------------------------------------------

VERSION=0.1.0
SUBJECT=update_godaddy_dns


# --- Options processing -------------------------------------------
if [ $# -eq 0 ] ; then
    echo "Usage: command -hv <API Key> <API Secret> <Domain> <Subdomain> <IP>"
    exit 1;
fi

while getopts ":i:vh" optname
  do
    case "$optname" in
      "v")
        echo "Version $VERSION"
        exit 0;
        ;;
      "i")
        echo "-i argument: $OPTARG"
        ;;
      "h")
        echo "Usage: command -hv <API Key> <API Secret> <Domain> <Subdomain> <IP>"
        exit 0;
        ;;
      "?")
        echo "Unknown option $OPTARG"
        exit 0;
        ;;
      ":")
        echo "No argument value for option $OPTARG"
        exit 0;
        ;;
      *)
        echo "Unknown error while processing options"
        exit 0;
        ;;
    esac
  done

shift $(($OPTIND - 1))

# Read parameters
api_key=$1
api_secret=$2
domain=$3
subdomain=$4
ip=$5

# --- Locks -------------------------------------------------------
LOCK_FILE=/tmp/$SUBJECT.lock
if [ -f "$LOCK_FILE" ]; then
   echo "Script is already running"
   exit
fi

trap "rm -f $LOCK_FILE" EXIT
touch $LOCK_FILE

# --- Body --------------------------------------------------------
#  SCRIPT LOGIC GOES HERE
echo "Updating GoDaddy DNS with the following parameters:"
echo "---------------------------------------------------"
echo Key: $api_key
echo Secret: $api_secret
echo Domain: $domain
echo Subdomain: $subdomain
echo IP: $ip
echo "---------------------------------------------------"

# First handle the wildcard NS entry, i.e. *.subdomain.domain
curl -X GET -H "Authorization: sso-key $api_key:$api_secret" "https://api.godaddy.com/v1/domains/$domain/records/A/*.$subdomain?X-Shopper-Id=135704685&domain=$domain" > output.txt
existing_ns_records=`cat output.txt`

if [ "$existing_ns_records" == '[]' ]; then
    echo "No records found for subdomain $subdomain"
    echo "Adding record to IP $ip"
    curl --request PATCH -H "Authorization: sso-key $api_key:$api_secret" -H "Content-Type: application/json" -d "[{\"type\":\"A\",\"name\":\"*.$subdomain\",\"data\":\"$ip\",\"ttl\":600}]" "https://api.godaddy.com/v1/domains/$domain/records?X-Shopper-Id=135704685&domain=$domain"
    echo "Verifying successful update"
    curl -X GET -H "Authorization: sso-key $api_key:$api_secret" "https://api.godaddy.com/v1/domains/$domain/records/A/*.$subdomain?X-Shopper-Id=135704685&domain=$domain"
else
    echo "Existing records found for subdomain $subdomain"
    echo $existing_ns_records
    echo "Updating with IP $ip"
    curl --request PUT -H "Authorization: sso-key $api_key:$api_secret" -H "Content-Type: application/json" -d "[{\"type\":\"A\",\"name\":\"*.$subdomain\",\"data\":\"$ip\",\"ttl\":600}]" "https://api.godaddy.com/v1/domains/$domain/records/A/*.$subdomain?X-Shopper-Id=135704685&domain=$domain"
    echo "Verifying successful update"
    curl -X GET -H "Authorization: sso-key $api_key:$api_secret" "https://api.godaddy.com/v1/domains/$domain/records/A/*.$subdomain?X-Shopper-Id=135704685&domain=$domain"
fi

# Now handle the "root" entry for subdomain, i.e. subdomain.domain
curl -X GET -H "Authorization: sso-key $api_key:$api_secret" "https://api.godaddy.com/v1/domains/$domain/records/A/$subdomain?X-Shopper-Id=135704685&domain=$domain" > output1.txt
existing_ns_root_records=`cat output1.txt`

if [ "$existing_ns_root_records" == '[]' ]; then
    echo "No root records found for subdomain $subdomain"
    echo "Adding record to IP $ip"
    curl --request PATCH -H "Authorization: sso-key $api_key:$api_secret" -H "Content-Type: application/json" -d "[{\"type\":\"A\",\"name\":\"$subdomain\",\"data\":\"$ip\",\"ttl\":600}]" "https://api.godaddy.com/v1/domains/$domain/records?X-Shopper-Id=135704685&domain=$domain"
    echo "Verifying successful update"
    curl -X GET -H "Authorization: sso-key $api_key:$api_secret" "https://api.godaddy.com/v1/domains/$domain/records/A/$subdomain?X-Shopper-Id=135704685&domain=$domain"
else
    echo "Existing records found for subdomain $subdomain"
    echo $existing_ns_records
    echo "Updating with IP $ip"
    curl --request PUT -H "Authorization: sso-key $api_key:$api_secret" -H "Content-Type: application/json" -d "[{\"type\":\"A\",\"name\":\"$subdomain\",\"data\":\"$ip\",\"ttl\":600}]" "https://api.godaddy.com/v1/domains/$domain/records/A/$subdomain?X-Shopper-Id=135704685&domain=$domain"
    echo "Verifying successful update"
    curl -X GET -H "Authorization: sso-key $api_key:$api_secret" "https://api.godaddy.com/v1/domains/$domain/records/A/$subdomain?X-Shopper-Id=135704685&domain=$domain"
fi


# Retrieve current
#curl -X GET -H "Authorization: sso-key dKiJ3F3pMZbr_RaSPL4Gx5qxU4Z2zTkSkU3:RaSSbxpX66etqrGsJL4jMD" "https://api.godaddy.com/v1/domains/accezz.io/records/A/*.putt?X-Shopper-Id=135704685&domain=accezz.io"

# Update existing
#curl --request PUT -H "Authorization: sso-key dKiJ3F3pMZbr_RaSPL4Gx5qxU4Z2zTkSkU3:RaSSbxpX66etqrGsJL4jMD" -H "Content-Type: application/json" -d "[{\"type\":\"A\",\"name\":\"*.putt\",\"data\":\"35.157.68.159\",\"ttl\":600}]" "https://api.godaddy.com/v1/domains/accezz.io/records/A/*.putt?X-Shopper-Id=135704685&domain=accezz.io"

# Create a new NS record
#curl --request PATCH -H "Authorization: sso-key dKiJ3F3pMZbr_RaSPL4Gx5qxU4Z2zTkSkU3:RaSSbxpX66etqrGsJL4jMD" -H "Content-Type: application/json" -d "[{\"type\":\"A\",\"name\":\"*.$NEW_DOMAIN\",\"data\":\"$NEW_IP\",\"ttl\":600}]" "https://api.godaddy.com/v1/domains/accezz.io/records?X-Shopper-Id=135704685&domain=accezz.io"

# Key: dKiJ3F3pMZbr_RaSPL4Gx5qxU4Z2zTkSkU3
# Secret: RaSSbxpX66etqrGsJL4jMD
# Domain: accezz.io
# Subdomain: avir
# IP: 10.10.10.90

# -----------------------------------------------------------------

