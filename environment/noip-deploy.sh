#!/usr/bin/env bash

# This is a TEMPORARY script that downloads, builds and configures the NoIP client on an Accezz.io PoD machine. It uses a pre-supplied
# script with NoIP Configuration (/tmp/noip-make.exp).

# Install pre-conditions needed to download, build and configure NoIP Dynamic Update Client on the machine
sudo apt install -y make gcc expect # Make for building/configuration, gcc for compilation, expect for non-interactive configuration script

# Download and extract the NoIP Dynamic Update Client
cd /usr/local/src/
sudo wget http://www.no-ip.com/client/linux/noip-duc-linux.tar.gz
echo "Extract the archive"
sudo tar xf noip-duc-linux.tar.gz

# Build and configure the NoIP Dynaic Update Client
echo "Go to sources directory"
cd noip-2.1.9-1/
echo "Automate the configuration"
sudo chmod +x /tmp/noip-make.exp
echo "Executing the expect script"
sudo expect -d /tmp/noip-make.exp

# Run the NoIP Dynamic Update Client
sudo /usr/local/bin/noip2