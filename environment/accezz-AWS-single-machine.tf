# Access credentials for a specific Terraform user
provider "aws" {
  access_key = "AKIAI7N54P6JWDAAUT5Q"
  secret_key = "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"
  #region     = "us-west-2"
  region     = "eu-central-1"
}

# Automatically retrieve the AMI of the latest accezz-io-pod image
data "aws_ami" "latest_build_ami" {
  most_recent = true
  
  filter {
    name = "name"
    values = ["accezz-io-pod*"]
  }
  owners = ["self"]
}

# Key pair for accessing the resulting machine with SSH
resource "aws_key_pair" "deployer" {
  key_name = "accezz_pod_key" 
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDdil6ElujId5cOfVpzhig2CLwnBb0oe+X1Sphw8P35Ez1KxKnX9xdRuh7QBhATfuiM1xVQDvT85408YibPLvqZBUFcFFqoGs+HFlkNe4Gz2xF8nrAkejbUQtxUcsUtIso/uT4ak7gqXxPPqPli9j4d7/9RBGBNhMyRijTIaA7fFwq+2UTTThcH9snydKTMoJwx+U9OMfAeshTT9l7z+zRHj9mjb9cA/oArP4wGmrireuq6Ai0qk5/iLnkxy+R8inKEawyZZnJpKu/HWQhpd9I2MflLczSCN105cWOdiuSSP+RHlk0Mp1KzlIIkfyzMV9uWKWpitVsuAAW/3eWaAW6V leoni@DESKTOP-AKG40T1"
}

# Security group for the Accezz.io POD Machine
resource "aws_security_group" "accezz-io-Web-Server" {
    name        = "Accezz-IO-Web-Server"
    description = "Security Group for AccezzIO Web Server"

    ingress {
        from_port       = 80
        to_port         = 80
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    ingress {
        from_port       = 8080
        to_port         = 8080
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    ingress {
        from_port       = 22
        to_port         = 22
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    ingress {
        from_port       = 443
        to_port         = 443
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }


    egress {
        from_port       = 0
        to_port         = 65535
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    egress {
        from_port       = 0
        to_port         = 65535
        protocol        = "udp"
        cidr_blocks     = ["0.0.0.0/0"]
    }

}

# Create the instance based on the chosen AMI
resource "aws_instance" "Accezz_io_PoD" {
  ami = "${data.aws_ami.latest_build_ami.id}"
  instance_type = "t2.micro"
  security_groups = ["Accezz-IO-Web-Server"]
  key_name = "accezz_pod_key"

  tags {
        Name = "ActiveAccezzIOPod"
  }

  # Start the containers environment and run the Dynamic DNS Updater Service
  provisioner "remote-exec" {

        inline = [
        "sudo /usr/local/bin/noip2",
        "sudo docker login -e dev@accezz.io -u accezziohub -p *Accezz*",
        "sudo docker-compose -f /opt/accezz/accezz-pod-compose.yml up -d"
        ]

        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${file("accezz_pod_access_pair")}"
        }
  }
}

# Store the public IP of the created/running instance
output "ip" {
    value = "${aws_instance.Accezz_io_PoD.public_ip}"
}