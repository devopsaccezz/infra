# Define external variables to be taken from tfvars file
variable "access_key" {}
variable "secret_key" {}
variable "pod_region"  {}
variable "pod_name"    {}
variable "godaddy_access_key" {}
variable "godaddy_secret" {}
variable "pod_dns_suffix" {}
variable "auth0_client_id" {}
variable "auth0_client_secret" {}
variable "auth0_database_connection" {}
variable "db_user" {}
variable "db_pwd" {}

# Access credentials for a specific Terraform user
provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.pod_region}"
}