#!/bin/bash

# This script assumes the following arguments:
# $1 - User ID for accessing the S3 bucket
# #2 - Secret Access Key for the above User
# $3 - Name of the S3 Bucket to mount

echo "Mounting S3 volumes"
echo "About to mount S3 Bucket: $3"

sudo echo $1:$2 > /etc/passwd-s3fs
sudo chmod 600 /etc/passwd-s3fs

sudo s3fs -o use_cache=/tmp/cache -o passwd_file=/etc/passwd-s3fs $3 /mnt/s3
if [ ! $? -eq 0 ]; then
        echo "Error mounting S3 Bucket"
        exit 1
fi

# Now install the init.d task for mounting the device upon system restart

if [ ! -f /etc/init.d/mount-certstore ]; then

        echo "Creating init.d script for mounting certstore"

        if [ ! -f /usr/local/accezz/mount-certstore ];then
                sudo cp /tmp/mount-certstore /usr/local/accezz/
        fi

        sudo sed -i "s/BUCKETNAME/$3/g" /usr/local/accezz/mount-certstore
        sudo cp /usr/local/accezz/mount-certstore /etc/init.d/
        sudo chmod 755 /etc/init.d/mount-certstore

else
        echo "init.d script for mounting certstore bucket exists"
fi