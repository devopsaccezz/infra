#!/bin/bash

# Param 1 - Host Address of PostgreSQL Database

echo "Database Connection: $1"

# Get PostgreSQL client
apt-get install -y postgresql-client

# Install JRE for running the db-migration tool
apt-get install -y openjdk-8-jre-headless

# Get the DB migration tool
echo
CIRCLE_CI_TOKEN=84696ad277287a1ad98b91f5a5fd9c3839ab8c06
DB_MIGRATION_BRANCH=master
echo "-----> Downloading db-migration tool..."
echo "-----> Getting download URL. Execting: curl --silent --write-out %{http_code} \"https://circleci.com/api/v1.1/project/bitbucket/accezz-io/db-migration/latest/artifacts?circle-token=${CIRCLE_CI_TOKEN}&branch=${DB_MIGRATION_BRANCH}&filter=successful\" | grep db-migration.jar | grep url| awk '{ print $3 }'  | tr -d '\"'"
db_migration_dl_url=`curl --silent --write-out %{http_code} "https://circleci.com/api/v1.1/project/bitbucket/accezz-io/db-migration/latest/artifacts?circle-token=${CIRCLE_CI_TOKEN}&branch=${DB_MIGRATION_BRANCH}&filter=successful" | grep db-migration.jar | grep url| awk '{ print $3 }'  | tr -d '"'`

echo
echo "-----> Executing: curl -o db-migration.jar ${db_migration_dl_url}?circle-token=${CIRCLE_CI_TOKEN}" 
curl -o db-migration.jar ${db_migration_dl_url}?circle-token=${CIRCLE_CI_TOKEN}

# Create databases
echo
echo "Initializing databases"
export PGPASSWORD=accpostgres123
psql -h $1 -U accpostgres --dbname=postgres -c "CREATE DATABASE cms"
psql -h $1 -U accpostgres --dbname=postgres -c "CREATE DATABASE mgmt"
psql -h $1 -U accpostgres --dbname=postgres -c "CREATE DATABASE ds"

# Run migration tool
echo
echo "-----> Creating CMS schema"
connection="data.source.connection.string = jdbc:postgresql://"
connection+=$1
connection+=":5432/cms"
echo "Connection: $connection"
echo $connection  > ./db-migration-conf.properties.cms
echo "db.username = accpostgres"  >> ./db-migration-conf.properties.cms
echo "db.password = accpostgres123" >> ./db-migration-conf.properties.cms
echo "db.name = cms" >>  ./db-migration-conf.properties.cms
java -jar db-migration.jar --migrate -f ./db-migration-conf.properties.cms

echo
echo "-----> Creating Management schema"
#echo "data.source.connection.string = jdbc:postgresql://$1:5432/${db.name}"  > ./db-migration-conf.properties.mgmt
connection1="data.source.connection.string = jdbc:postgresql://"
connection1+=$1
connection1+=":5432/mgmt"
echo $connection1  > ./db-migration-conf.properties.mgmt
echo "db.username = accpostgres"  >> ./db-migration-conf.properties.mgmt
echo "db.password = accpostgres123" >> ./db-migration-conf.properties.mgmt
echo "db.name = mgmt" >>  ./db-migration-conf.properties.mgmt
java -jar db-migration.jar --migrate -f ./db-migration-conf.properties.mgmt

echo
echo "-----> Creating Directory Services schema"
sudo docker run --rm -e "DATABASE_URL=postgres://accpostgres:accpostgres123@$1/ds" accezziohub/accezzio:directory-service-latest npm run db:migrate