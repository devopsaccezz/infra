#!/bin/bash

# Check if the block device has a filesystem or not
sudo file -s /dev/xvdh | grep -q "ext4 filesystem"
if [ $? -eq 0 ]; then
    echo "EBS Device contains a file system, will mount it to /mnt/logstore"
else

    # Check that this is a raw block data device
    sudo file -s /dev/xvdh | grep -q "xvdh: data"
    if [ $? -eq 0 ]; then
         echo "EBS Device does not contain a file system, will try to create one"

         # Create a file system on the device
         sudo mkfs -t ext4 /dev/xvdh
         if [ $? -eq 0 ]; then
                echo "Successfully created file system on the device"
         else
                echo "Error creating file system on the block device - exiting"
                exit 1
        fi
    else
         echo "EBS Device error"
         sudo file -s /dev/xvdh
         exit 1
    fi
fi

# Check if the mount directory exists and create it if needed
if [ ! -d /mnt/logstore ]; then
        echo "Creating /mnt/logstore mountpoint"
        sudo mkdir /mnt/logstore
fi

# Now try to mount the device
sudo mount /dev/xvdh /mnt/logstore
if [ ! $? -eq 0 ]; then
        echo "Error mounting the block storage"
        exit 1
fi

# Add entry to /etc/fstab, if neededfstab=/etc/fstab
if [[ $(grep -q "/mnt/logstore" /etc/fstab) ]]
then
    sudo echo "/dev/xvdh /mnt/logstore ext4 defaults 0 2" >> /etc/fstab
else
    echo "Entry in fstab exists."
fi

# Check if the required directory structure exists, create it if not
if [ ! -d /mnt/logstore/certs ]; then
        sudo mkdir /mnt/logstore/certs
fi
if [ ! -d /mnt/logstore/resty-frontend ]; then
        sudo mkdir /mnt/logstore/resty-frontend
fi
if [ ! -d /mnt/logstore/report ]; then
        sudo mkdir /mnt/logstore/report
fi
if [ ! -d /mnt/logstore/tcb ]; then
        sudo mkdir /mnt/logstore/tcb
fi

# Temporary patch for GoAccess container - create a non-empty access.log file
sudo sh -c 'sudo echo "159.203.182.22 - Unknown User [07/Mar/2017:14:45:09 +0000] \"GET / HTTP/1.1\" 404 3558 \"-\" \"Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/98 Safari/537.4" > /mnt/logstore/resty-frontend/access.log'

echo "Storage volume prepared successfully!"
#ubuntu@ip-172-31-21-23:/mnt$ sudo file -s /dev/xvdh
#/dev/xvdh: data

#ubuntu@ip-172-31-21-23:/mnt$ sudo mkfs -t ext4 /dev/xvdh
#mke2fs 1.43.3 (04-Sep-2016)
#Creating filesystem with 13107200 4k blocks and 3276800 inodes
#Filesystem UUID: 491121d1-1b47-4ba0-b5b4-dfeb187c1c2e
#Superblock backups stored on blocks:
#        32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208,
#        4096000, 7962624, 11239424

#Allocating group tables: done
#Writing inode tables: done
#Creating journal (65536 blocks): done
#Writing superblocks and filesystem accounting information: done


#ubuntu@ip-172-31-21-23:/mnt$ sudo mount /dev/xvdh /mnt/logstore/

#ubuntu@ip-172-31-21-23:/mnt$ sudo file -s /dev/xvdh
#/dev/xvdh: Linux rev 1.0 ext4 filesystem data, UUID=491121d1-1b47-4ba0-b5b4-dfeb187c1c2e (needs journal recovery) (extents) (64bit) (large files) (huge files)