#/bin/bash

echo ***** Starting accezz.io services *****

sudo docker login -e dev@accezz.io -u accezziohub -p *Accezz*

if [ -f /tmp/env ]; then
    echo "Moving the generated environment settings file to /usr/local/accezz"
    sudo mv /tmp/env /usr/local/accezz/.env
else
    echo "Env file not found!!!!!!!"
fi

cd /usr/local/accezz
echo "Starting accezz.io PoD services"

sudo docker-compose -f /usr/local/accezz/accezz-pod-compose.yml up -d
sudo docker logout

echo ***************************************
