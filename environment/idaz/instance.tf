# create a virtual network
resource "azurerm_virtual_network" "accezzterraformnetwork" {
    name = "${var.pod_name}_vn${random_id.instanceid.dec}"
    address_space = ["10.0.0.0/16"]
    location = "West US"
    resource_group_name = "${azurerm_resource_group.accezz_rg.name}"
}

# create subnet
resource "azurerm_subnet" "accezzterraformsubnet" {
    name = "${var.pod_name}_sub${random_id.instanceid.dec}"
    resource_group_name = "${azurerm_resource_group.accezz_rg.name}"
    virtual_network_name = "${azurerm_virtual_network.accezzterraformnetwork.name}"
    address_prefix = "10.0.2.0/24"
}

# create public IP
resource "azurerm_public_ip" "azureterraformips" {
    name = "${var.pod_name}_accezzip${random_id.instanceid.dec}"
    location = "West US"
    resource_group_name = "${azurerm_resource_group.accezz_rg.name}"
    public_ip_address_allocation = "static" # Important to use static here, otherwise IP is always re-allocated when a machine is being started. Alternative workaround: host = "${element(azurerm_public_ip.client.*.fqdn, count.index)}"

    tags {
        environment = "AccezzIO PoD"
    }
}

# create network interface
resource "azurerm_network_interface" "accezznic" {
    name = "${var.pod_name}_nic${random_id.instanceid.dec}"
    location = "West US"
    resource_group_name = "${azurerm_resource_group.accezz_rg.name}"

    ip_configuration {
        name = "testconfiguration1"
        subnet_id = "${azurerm_subnet.accezzterraformsubnet.id}"
        private_ip_address_allocation = "static"
        private_ip_address = "10.0.2.5"
        public_ip_address_id = "${azurerm_public_ip.azureterraformips.id}"
    }
}

# create virtual machine
resource "azurerm_virtual_machine" "accezzvm" {
    name = "${var.pod_name}_accezzvm${random_id.instanceid.dec}"
    location = "West US"
    resource_group_name = "${azurerm_resource_group.accezz_rg.name}"
    network_interface_ids = ["${azurerm_network_interface.accezznic.id}"]
    #vm_size = "Standard_A0"
    vm_size = "Standard_L4S"

    storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        #sku       = "14.04.2-LTS"
        sku        = "16.04-LTS"
        version   = "latest"
    }

    storage_os_disk {
        name = "${var.pod_name}_accdisk${random_id.instanceid.dec}"
        vhd_uri = "${azurerm_storage_account.accezzterraformstorage.primary_blob_endpoint}${azurerm_storage_container.accezzstoragestoragecontainer.name}/myosdisk.vhd"
        caching = "ReadWrite"
        create_option = "FromImage"
    }

    os_profile {
        computer_name = "accezzpod"
        admin_username = "ubuntu"
        admin_password = "*Accezz1234*"
    }

    os_profile_linux_config {
        disable_password_authentication = false
    }

    tags {
        environment = "staging"
    }

    provisioner "file" {
        source = "./instance_management/accezz-pod-compose.yml"
        destination = "/tmp/accezz-pod-compose.yml"
        connection {
            type     = "ssh"
            user     = "ubuntu"
            password = "*Accezz1234*"
            host = "${azurerm_public_ip.azureterraformips.ip_address}"
        }
    }
    provisioner "file" {
        source = "./instance_management/mount-certstore"
        destination = "/tmp/mount-certstore"
        connection {
            type     = "ssh"
            user     = "ubuntu"
            password = "*Accezz1234*"
            host = "${azurerm_public_ip.azureterraformips.ip_address}"            
        }        
    }   
    provisioner "file" {
        source = "./remote_provisioners/pre-destruct.sh"
        destination = "/tmp/pre-destruct.sh"
        connection {
            type     = "ssh"
            user     = "ubuntu"
            password = "*Accezz1234*"
            host = "${azurerm_public_ip.azureterraformips.ip_address}"            
        }        
    }   
    # provisioner "remote-exec" {
    #     script = "./remote_provisioners/mount-ebs-volume.sh"      
    #     connection {
    #         type     = "ssh"
    #         user     = "ubuntu"
    #         password = "*Accezz1234*"
    #     }        
    # }  
    provisioner "remote-exec" {
        script = "./remote_provisioners/install-docker-and-containers.sh"      
        connection {
            type     = "ssh"
            user     = "ubuntu"
            password = "*Accezz1234*"
            host = "${azurerm_public_ip.azureterraformips.ip_address}"            
        }        
    }        

    # Create /Update GoDaddy DNS Records using File/Remote provisioners (useful when running Terraform on Windows/Cygwin)
    provisioner "file" { # We need to copy the file first in order to provide it with arguments - devops user credentials
        source = "./local_provisioners/update_godaddy_dns.sh"
        destination = "/tmp/update_godaddy_dns.sh"
        connection {
            type     = "ssh"
            user     = "ubuntu"
            password = "*Accezz1234*"
            host = "${azurerm_public_ip.azureterraformips.ip_address}"            
        }          
    }
    provisioner "remote-exec" { # Now execute the script passing devops user's access and secret as arguments - the script will need it to mount the S3 bucket'
        inline = [
            "sudo chmod +x /tmp/update_godaddy_dns.sh",
            "/tmp/update_godaddy_dns.sh ${var.godaddy_access_key} ${var.godaddy_secret} accezz.io ${var.pod_name} ${azurerm_public_ip.azureterraformips.ip_address}"  
        ]
        connection {
            type     = "ssh"
            user     = "ubuntu"
            password = "*Accezz1234*"
            host = "${azurerm_public_ip.azureterraformips.ip_address}"            
        }        
    }

    provisioner "remote-exec" {
        script = "./remote_provisioners/prepare-directories.sh"      
        connection {
            type     = "ssh"
            user     = "ubuntu"
            password = "*Accezz1234*"
            host = "${azurerm_public_ip.azureterraformips.ip_address}"            
        }        
    } 

    # Save IP in local file for future use
    provisioner "local-exec" {
        command = "echo ${azurerm_public_ip.azureterraformips.ip_address} > ./ip"
    }

    provisioner "remote-exec" {
        script = "./remote_provisioners/initialize-docker-gc.sh"      
        connection {
            type     = "ssh"
            user     = "ubuntu"
            password = "*Accezz1234*"
            host = "${azurerm_public_ip.azureterraformips.ip_address}"            
        }        
    }     

    provisioner "remote-exec" {
        script = "./remote_provisioners/install-db-on-host.sh"      
        connection {
            type     = "ssh"
            user     = "ubuntu"
            password = "*Accezz1234*"
            host = "${azurerm_public_ip.azureterraformips.ip_address}"            
        }        
    } 
    # Initialize the database upon creation
    provisioner "file" { # We need to copy the file first in order to provide it with arguments - devops user credentials
        source = "./remote_provisioners/initialize-db.sh"
        destination = "/tmp/initialize-db.sh"
        connection {
            type     = "ssh"
            user     = "ubuntu"
            password = "*Accezz1234*"
            host = "${azurerm_public_ip.azureterraformips.ip_address}"            
        }          
    }
    provisioner "remote-exec" { # Now execute the script 
        inline = [
            "sudo chmod +x /tmp/initialize-db.sh",
            "sudo /tmp/initialize-db.sh localhost"  
        ]
        connection {
            type     = "ssh"
            user     = "ubuntu"
            password = "*Accezz1234*"
            host = "${azurerm_public_ip.azureterraformips.ip_address}"            
        }        
    }
   
    # Add the environment settings to the .env file for docker-compose
    provisioner "remote-exec" { 
        inline = [
            "sudo echo Preparing .env file for docker-compose",
            "sudo echo POD_NAME=${var.pod_name} >> /tmp/env",
            "sudo echo POD_DNS_SUFFIX=${var.pod_dns_suffix} >> /tmp/env",
            "sudo echo POD_AUTH0_CLIENT_ID=${var.auth0_client_id} >> /tmp/env",
            "sudo echo POD_AUTH0_CLIENT_SECRET=${var.auth0_client_secret} >> /tmp/env",
            "sudo echo POD_AUTH0_DATABASE_CONNECTION=${var.auth0_database_connection} >> /tmp/env",
            "sudo echo DB_ADDRESS=172.17.0.1 >> /tmp/env",
            "sudo echo DB_USER=${var.db_user} >> /tmp/env",
            "sudo echo DB_PWD=${var.db_pwd} >> /tmp/env"            
        ]
        connection {
            type     = "ssh"
            user     = "ubuntu"
            password = "*Accezz1234*"
            host = "${azurerm_public_ip.azureterraformips.ip_address}"            
        }      
    }    

    # Start our services on the machine
    provisioner "remote-exec" { 
        script = "./remote_provisioners/start-services.sh"
        connection {
            type     = "ssh"
            user     = "ubuntu"
            password = "*Accezz1234*"
            host = "${azurerm_public_ip.azureterraformips.ip_address}"            
        }       
    }



}
