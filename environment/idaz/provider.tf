# Define external variables to be taken from tfvars file
variable "azure_client_id" {}
variable "azure_client_secret" {}
variable "azure_subscription_id" {}
variable "azure_tenant_id" {}
variable "pod_region"  {}
variable "pod_name"    {}
variable "godaddy_access_key" {}
variable "godaddy_secret" {}
variable "pod_dns_suffix" {}
variable "auth0_client_id" {}
variable "auth0_client_secret" {}

# Configure the Microsoft Azure Provider
provider "azurerm" {
  subscription_id = "${var.azure_subscription_id}"
  client_id       = "${var.azure_client_id}"
  client_secret   = "${var.azure_client_secret}"
  tenant_id       = "${var.azure_tenant_id}"
}

# create a resource group 
resource "azurerm_resource_group" "accezz_rg" {
    name = "${var.pod_name}_accezz_rg_${random_id.instanceid.dec}"
    location = "West US"
}

# Create a random ID for this instance - needed because Azure has a lockdown period for a names of destroyed resources
resource "random_id" "instanceid" {
    byte_length = 3
}