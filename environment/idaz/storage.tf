# create storage account
resource "azurerm_storage_account" "accezzterraformstorage" {
    name = "${var.pod_name}accstor${random_id.instanceid.dec}"
    resource_group_name = "${azurerm_resource_group.accezz_rg.name}"
    location = "westus"
    account_type = "Standard_GRS" #LRS

    tags {
        environment = "staging"
    }
}

# create storage container
resource "azurerm_storage_container" "accezzstoragestoragecontainer" {
    name = "${var.pod_name}acccont${random_id.instanceid.dec}"
    resource_group_name = "${azurerm_resource_group.accezz_rg.name}"
    storage_account_name = "${azurerm_storage_account.accezzterraformstorage.name}"
    container_access_type = "private"
    #depends_on = ["azurerm_storage_account.accezzterraformstorage"]
}

