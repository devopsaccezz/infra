# Define a managed PostgreSQL DB Instance
resource "aws_db_instance" "postgresinstance" 
{
    identifier                = "${var.pod_name}postgresinstance"
    allocated_storage         = 5
    storage_type              = "gp2"
    engine                    = "postgres"
    engine_version            = "9.6.1"
    instance_class            = "db.t2.micro"
    name                      = ""
    username                  = "${var.db_user}"
    password                  = "${var.db_pwd}"
    port                      = 5432
    publicly_accessible       = true
    availability_zone         = "${var.pod_region}a"
    #security_group_names      =  ["${aws_security_group.db_security_group.name}"]
    #db_subnet_group_name      = "default"
    parameter_group_name      = "default.postgres9.6"
    multi_az                  = false
    backup_retention_period   = 7
    backup_window             = "21:35-22:05"
    maintenance_window        = "wed:03:47-wed:04:17"
    final_snapshot_identifier = "${var.pod_name}postgresinstance-final"
    vpc_security_group_ids = ["${aws_security_group.db_security_group.id}"]

    lifecycle {
         #prevent_destroy = true
         create_before_destroy = true
    }

    # Initialize the database upon creation
    provisioner "file" { # We need to copy the file first in order to provide it with arguments - devops user credentials
        source = "./remote_provisioners/initialize-db.sh"
        destination = "/tmp/initialize-db.sh"
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${file("accezz_pod_access_pair")}"
            host = "${aws_instance.accezz_io_PoD.public_ip}"
        }         
    }
    provisioner "remote-exec" { # Now execute the script 
        inline = [
            "sudo chmod +x /tmp/initialize-db.sh",
            "sudo /tmp/initialize-db.sh ${aws_db_instance.postgresinstance.address}"  
        ]
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${file("accezz_pod_access_pair")}"
            host = "${aws_instance.accezz_io_PoD.public_ip}"
        }       
    }

    # Now add the database address to the .env file for docker-compose
    provisioner "remote-exec" { # Now execute the script 
    inline = [
        "sudo echo DB_ADDRESS=${aws_db_instance.postgresinstance.address} >> /tmp/env"  
    ]
    connection {
        type = "ssh"
        user = "ubuntu"
        private_key = "${file("accezz_pod_access_pair")}"
        host = "${aws_instance.accezz_io_PoD.public_ip}"
        }       
    }

}

resource "aws_security_group" "db_security_group" {
    name        = "${var.pod_name}-accezz-db-security"
    description = "Security group for the accezz.io database"

    lifecycle {
         #prevent_destroy = true
         create_before_destroy = true
    }

    ingress {
        from_port       = 5432
        to_port         = 5432
        protocol        = "tcp"
        #cidr_blocks     = ["${aws_instance.accezz_io_PoD.private_ip}/0"]
        cidr_blocks     = ["0.0.0.0/0"]
    }


    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

}

# Store the private IP of the created database instance
output "Private_DB_IP" {
    value = "${aws_db_instance.postgresinstance.address}"
}
