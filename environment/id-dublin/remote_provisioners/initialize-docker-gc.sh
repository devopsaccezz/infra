#!/bin/bash

# This script installs Garbage Collector for Docker images and containers (implemented by Spotify) and registers it as cron job for weekly run
sudo apt-get install git devscripts debhelper build-essential dh-make
cd /tmp
git clone https://github.com/spotify/docker-gc.git
cd docker-gc
debuild -us -uc -b
cd ..
sudo dpkg -i docker-gc_0.1.0_all.deb

echo "Creating cron job for garbage collector of Docker containers and images"
    
sudo echo "#!/bin/bash" > /tmp/accezz-gc
sudo echo "/usr/sbin/docker-gc" >> /tmp/accezz-gc
sudo mv /tmp/accezz-gc /etc/cron.weekly/accezz-gc
sudo chmod +x /etc/cron.daily/accezz-gc