#/bin/bash

# Prepare for mounting S3FS

echo "Preparing directories"

if [ ! -d /mnt/cache ]; then
    echo "Making /tmp/cache"
    sudo mkdir /tmp/cache # To be used as cache for S3FS
    sudo chmod 777 /tmp/cache
fi

if [ ! -d /mnt/s3 ]; then
    echo "Making /mnt/s3"
    sudo mkdir /mnt/s3 # To mount to, use any path you want
fi

if [ ! -d /mnt/logstore ]; then
    echo "Making /mnt/logstore"
    sudo mkdir /mnt/logstore # To mount to, use any path you want
fi

