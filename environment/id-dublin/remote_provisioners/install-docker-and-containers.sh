#!/usr/bin/env bash

# This is a temporary provisioning file for the Accezz.io development environment VMs/Instances. It uses non-declarative Bash shell script to prepare a
# Ubuntu machine running Docker Engine and Docker Compose. 

echo ****************** Provisioning of Accezz.io PoD Image Start **************************
#sleep 15

sudo apt-get update

# Install curl
sudo apt-get install -y curl unzip

# Get PostgreSQL client (for database initialization)
sudo apt-get install -y postgresql-client

# Install JRE for running the db-migration tool
sudo apt-get install -y openjdk-8-jre-headless

# Install Docker Engine - This is the previous version of docker engine, prior to the change to Community Edition / Enterprise Edition
# echo Installing Docker Engine
# sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
# sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'
# sudo apt-get update
# sudo apt-cache policy docker-engine
# sudo apt-get install -y docker-engine
# sudo systemctl status docker

# Install Docker Community Edition
echo Installing Docker CE
echo "Installing pre-conditions"
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common  # Install pre-requirements
echo "Retrieving repository key"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -              # Install GPG Key for Docker repository
sudo apt-key fingerprint 0EBFCD88
echo "Adding repository"
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
echo "Adding Docker CE"
sudo apt-get update
sudo apt-cache policy docker-ce
sudo apt-get install -y docker-ce
sudo docker run hello-world                                                               # Check for test message

# Install Docker Compose
echo Installing Docker Compose
sudo curl -L https://github.com/docker/compose/releases/download/1.9.0/docker-compose-`uname -s`-`uname -m` > /tmp/docker-compose
sudo chmod +x /tmp/docker-compose
sudo cp /tmp/docker-compose /usr/local/bin/docker-compose


# Prepare the directory structure for running the environment
if [ -f /tmp/accezz-pod-compose.yml ]
then
    echo "Moving the Accezz environment files to /usr/local/accezz"
    sudo mkdir /usr/local/accezz
    sudo cp /tmp/accezz-pod-compose.yml /usr/local/accezz/accezz-pod-compose.yml
fi

# Prepare the directory structure for running the environment
if [ -f /tmp/pre-destruct.sh ]
then
    echo "Moving the pre-destruct script /usr/local/accezz"
    sudo cp /tmp/pre-destruct.sh /usr/local/accezz/pre-destruct.sh
    sudo chmod +x /usr/local/accezz/pre-destruct.sh
fi

# Install CTOP container management tool
echo "Installing ctop"
sudo wget https://github.com/bcicen/ctop/releases/download/v0.4.1/ctop-0.4.1-linux-amd64 -O ctop
sudo mv ctop /usr/local/bin/
sudo chmod +x /usr/local/bin/ctop

# Install Consul CLI (for troubleshooting)
echo "Installing Consul"
sudo wget https://releases.hashicorp.com/consul/0.7.5/consul_0.7.5_linux_amd64.zip?_ga=1.224359195.1416660606.1480325657 -O consul.zip
sudo unzip consul.zip
sudo mv consul /usr/local/bin/
sudo chmod +x /usr/local/bin/consul

# Install REDIS CLI (for troubleshooting)
echo "Installing Redis"
wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable
make
sudo make install

# Update setting needed for Elastic Seach and set them for restart
# According to: https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html
sudo sysctl -w vm.max_map_count=262144
sudo sh -c 'echo "vm.max_map_count = 262144" > /etc/sysctl.com'


# Pull the relevant containers into the image from Docker Hub
echo ***** Pulling Container Images ***** 
sudo docker login -e dev@accezz.io -u accezziohub -p *Accezz*
sudo docker-compose -f /usr/local/accezz/accezz-pod-compose.yml pull # - Do not start our containers in the image, will be done in Terraform provisioner
sudo docker logout
echo ************************************


echo ****************** Provisioning of Accezz.io PoD Image End **************************
