#/bin/bash

if [ ! -f ip ]; then
    echo "ip file missing, Did terraform apply run?"
    exit 1
fi

read value < ip
connection=`echo ubuntu@$value`
ssh -i "accezz_pod_access_pair" $connection "sudo /usr/local/accezz/pre-destruct.sh"
