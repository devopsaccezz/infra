# Automatically retrieve the AMI of the latest accezz-io-pod image
data "aws_ami" "latest_build_ami" {
  most_recent = true
  
  filter {
    name = "name"
    values = ["accezz-io-pod*"]
  }
  owners = ["self"]
}

# Search for an EBS volume created for this PoD
data "aws_ebs_volume" "ebs_volume" {
    depends_on = ["aws_ebs_volume.logstore"]
    most_recent = true
    filter {
        name = "tag:Name"
        values = ["${var.pod_name}-logstore"]
    }
}

# Key pair for accessing the resulting machine with SSH
resource "aws_key_pair" "deployer" {
  key_name = "${var.pod_name}_access_pair" 
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDdil6ElujId5cOfVpzhig2CLwnBb0oe+X1Sphw8P35Ez1KxKnX9xdRuh7QBhATfuiM1xVQDvT85408YibPLvqZBUFcFFqoGs+HFlkNe4Gz2xF8nrAkejbUQtxUcsUtIso/uT4ak7gqXxPPqPli9j4d7/9RBGBNhMyRijTIaA7fFwq+2UTTThcH9snydKTMoJwx+U9OMfAeshTT9l7z+zRHj9mjb9cA/oArP4wGmrireuq6Ai0qk5/iLnkxy+R8inKEawyZZnJpKu/HWQhpd9I2MflLczSCN105cWOdiuSSP+RHlk0Mp1KzlIIkfyzMV9uWKWpitVsuAAW/3eWaAW6V leoni@DESKTOP-AKG40T1"
    
    lifecycle {
            create_before_destroy = true
    }

}

# Security group for the Accezz.io POD Machine
resource "aws_security_group" "sg-accezz-io-pod" {
    name        = "${var.pod_name}-security-group-1"
    description = "Security Group for AccezzIO PoD for ${var.pod_name}"

    ingress {
        from_port       = 80
        to_port         = 80
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    ingress {
        from_port       = 8080
        to_port         = 8080
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    ingress {
        from_port       = 22
        to_port         = 22
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    ingress {
        from_port       = 443
        to_port         = 443
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }


    egress {
        from_port       = 0
        to_port         = 65535
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    egress {
        from_port       = 0
        to_port         = 65535
        protocol        = "udp"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    lifecycle {
         #prevent_destroy = true
         create_before_destroy = true
    }

}

# Create the instance based on the chosen AMI
resource "aws_instance" "accezz_io_PoD" {

    ami = "${data.aws_ami.latest_build_ami.id}"
    #ami = "ami-1f83aa79"
    availability_zone = "${var.pod_region}a"  
    instance_type = "m4.xlarge"
    security_groups = ["${aws_security_group.sg-accezz-io-pod.name}"]
    key_name = "${var.pod_name}_access_pair"

    tags {
            Name = "${var.pod_name}_ActiveAccezzIOPod"
    }

    # Save IP in local file for future use
    provisioner "local-exec" {
        command = "echo ${aws_instance.accezz_io_PoD.public_ip} > ./ip"
    }

    lifecycle {
         #prevent_destroy = true
         create_before_destroy = true
    }

    # Create / Update GoDaddy DNS records - this is a local provisioner to be run when a provisioning is run from Unix machine. In Cygwin, it stil runs local provisioners with "cmd /c"
    # provisioner "local-exec" {
    #     command = "./local_provisioners/update_godaddy_dns.sh ${var.godaddy_access_key} ${var.godaddy_secret} accezz.io ${var.pod_name} ${aws_instance.accezz_io_PoD.public_ip}"
    # } 

    # Create /Update GoDaddy DNS Records using File/Remote provisioners (useful when running Terraform on Windows/Cygwin)
    provisioner "file" { # We need to copy the file first in order to provide it with arguments - devops user credentials
        source = "./local_provisioners/update_godaddy_dns.sh"
        destination = "/tmp/update_godaddy_dns.sh"
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${file("accezz_pod_access_pair")}"
            host = "${aws_instance.accezz_io_PoD.public_ip}"
        }         
    }
    provisioner "remote-exec" { # Now execute the script passing devops user's access and secret as arguments - the script will need it to mount the S3 bucket'
        inline = [
            "sudo chmod +x /tmp/update_godaddy_dns.sh",
            "/tmp/update_godaddy_dns.sh ${var.godaddy_access_key} ${var.godaddy_secret} accezz.io ${var.pod_name} ${aws_instance.accezz_io_PoD.public_ip}"  
        ]
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${file("accezz_pod_access_pair")}"
            host = "${aws_instance.accezz_io_PoD.public_ip}"
        }       
    }    
}

# Attach the EBS Volume to the PoD instance
resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/xvdh"
  volume_id = "${aws_ebs_volume.logstore.id}"
  instance_id = "${aws_instance.accezz_io_PoD.id}"
  depends_on = ["aws_db_instance.postgresinstance"] # We're initializing the environment after both the database and the volume attachment have completed

    lifecycle {
         #prevent_destroy = true
         create_before_destroy = true
    }

    # Mount the EBS volume
    provisioner "remote-exec" {
        script = "./remote_provisioners/mount-ebs-volume.sh"
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${file("accezz_pod_access_pair")}"
            host = "${aws_instance.accezz_io_PoD.public_ip}"
        }
    }
    
    # Prepare the required directories on the instance drive
    provisioner "remote-exec" {
        script = "./remote_provisioners/prepare-directories.sh"
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${file("accezz_pod_access_pair")}"
            host = "${aws_instance.accezz_io_PoD.public_ip}"
        }
    }
    # Copy the init.d script for S3 bucket mounting 
    provisioner "file" {
        source = "./instance_management/mount-certstore"
        destination = "/tmp/mount-certstore"
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${file("accezz_pod_access_pair")}"
            host = "${aws_instance.accezz_io_PoD.public_ip}"
        }         
    }
    # Mount the S3 volume use for backup of "stateful" information, such as HTTPS certificates
    provisioner "file" { # We need to copy the file first in order to provide it with arguments - devops user credentials
        source = "./remote_provisioners/mount-s3-volume.sh"
        destination = "/tmp/mount-s3-volume.sh"
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${file("accezz_pod_access_pair")}"
            host = "${aws_instance.accezz_io_PoD.public_ip}"
        }         
    }
    provisioner "remote-exec" { # Now execute the script passing devops user's access and secret as arguments - the script will need it to mount the S3 bucket'
        inline = [
            "sudo chmod +x /tmp/mount-s3-volume.sh",
            "sudo /tmp/mount-s3-volume.sh ${var.access_key} ${var.secret_key} ${aws_s3_bucket.certstore.tags.Name}"  
        ]
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${file("accezz_pod_access_pair")}"
            host = "${aws_instance.accezz_io_PoD.public_ip}"
        }       
    }  

    # Add the environment settings to the .env file for docker-compose
    provisioner "remote-exec" { 
    inline = [
        "sudo echo Preparing .env file for docker-compose",
        "sudo echo POD_NAME=${var.pod_name} >> /tmp/env",
        "sudo echo POD_DNS_SUFFIX=${var.pod_dns_suffix} >> /tmp/env",
        "sudo echo POD_AUTH0_CLIENT_ID=${var.auth0_client_id} >> /tmp/env",
        "sudo echo POD_AUTH0_CLIENT_SECRET=${var.auth0_client_secret} >> /tmp/env",
        "sudo echo POD_AUTH0_DATABASE_CONNECTION=${var.auth0_database_connection} >> /tmp/env",
        "sudo echo POD_AUTH0_LOGS_CLIENT_ID=${var.pod_auth0_logs_client_id} >> /tmp/env",
        "sudo echo POD_AUTH0_CLIENT_LOGS_SECRET=${var.pod_auth0_client_logs_secret} >> /tmp/env",
        "sudo echo DB_ADDRESS=${aws_db_instance.postgresinstance.address} >> /tmp/env", 
        "sudo echo DB_USER=${var.db_user} >> /tmp/env",
        "sudo echo DB_PWD=${var.db_pwd} >> /tmp/env"
    ]
    connection {
        type = "ssh"
        user = "ubuntu"
        private_key = "${file("accezz_pod_access_pair")}"
        host = "${aws_instance.accezz_io_PoD.public_ip}"
        }       
    }    

    # Start our services on the machine
    provisioner "remote-exec" { 
        script = "./remote_provisioners/start-services.sh"
        connection {
            type = "ssh"
            user = "ubuntu"
            private_key = "${file("accezz_pod_access_pair")}"
            host = "${aws_instance.accezz_io_PoD.public_ip}"
        }      
    }

}

# Store the public IP of the created/running instance
output "Public_PoD_IP" {
    value = "${aws_instance.accezz_io_PoD.public_ip}"
}

