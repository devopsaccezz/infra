#/bin/bash

# Stop our containers if they are running
echo "Stopping Containers"
sudo docker-compose -f /usr/local/accezz/accezz-pod-compose.yml stop # - Do not start our containers in the image, will be done in Terraform provisioner

# TODO: Back up the latest state of certificates directory to the S3 bucket

# Unmount the EBS volume (to allow destruction)
echo "Unmounting the EBS and S3 Volumes"
sudo umount /mnt/logstore

