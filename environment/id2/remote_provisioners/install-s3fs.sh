#/bin/bash

# Provision the open source S3FS user-mode FileSystem allowing mounting of the AWS S3 Bucket on the machine
# Source Repository: https://github.com/s3fs-fuse/s3fs-fuse
# Installation Procedure from: https://fullstacknotes.com/mount-aws-s3-bucket-to-ubuntu-file-system/

# Install pre-conditions
echo ***** Installing S3FS Pre-Conditions ***** 
sudo apt-get install -y build-essential git libfuse-dev libcurl4-openssl-dev libxml2-dev mime-support automake libtool
sudo apt-get install -y pkg-config libssl-dev
echo ************************************

# Get the sources tree and build it
echo ***** Building S3FS ***** 
git clone https://github.com/s3fs-fuse/s3fs-fuse
cd s3fs-fuse/
./autogen.sh
./configure --prefix=/usr --with-openssl
make
sudo make install

# Prepare to deal with the init.d task for automatic mounting, register the script for startup when first mounting the S3 volume when provisioning the instance (mount-s3-volume.sh)
sudo cp /tmp/mount-certstore /usr/local/accezz/


# This should be a cron job
if [ ! -f /etc/cron.daily/accezz-backup ]; then

    echo "Creating cron job for accezz.io certificate backup"
    
    sudo echo "#!/bin/bash" > /tmp/accezz-backup
    sudo echo "tar cvf /mnt/s3/`date '+%m%d%y%H%M%S'`backup.tar /mnt/logstore/certs/etc/resty-auto-ssl" >> /tmp/accezz-backup
    sudo mv /tmp/accezz-backup /etc/cron.daily/accezz-backup
    sudo chmod +x /etc/cron.daily/accezz-backup

else
    echo "Cron job for accezz.io certificate backup exists"
fi


echo ************************************


# Next Steps:
# 1. Prepare a file with access credentials: ~/.passwd-s3fs with content: <AWS Access Key ID>:<AWS Secret Access Key>
# 2. Run the user-mode file-system: s3fs -o use_cache=/tmp/cache mybucket /mnt/s3 # With mybucket is your bucket name